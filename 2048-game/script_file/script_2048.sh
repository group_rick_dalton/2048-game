#!/bin/bash

WORKDIR="/home/vagrant/2048-game/"
SCRIPTDIR="/home/vagrant/2048-game/script_file"

curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash - && sudo apt-get install -y nodejs

cd $WORKDIR

npm install --include=dev

npm audit fix

npm run build

sudo rm -rf /etc/nginx/nginx.conf

cd $SCRIPTDIR

sudo cp nginx.conf /etc/nginx/

sudo nginx -s reload

sudo cp 2048game.service /etc/systemd/system/

sudo systemctl enable 2048game.service

sudo systemctl daemon-reload

sudo systemctl start 2048game.service

