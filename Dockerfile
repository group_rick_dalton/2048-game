FROM node:18-alpine as builder
WORKDIR /src
COPY ./2048-game .
RUN npm install && npm run build

FROM nginx:alpine
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /src/dist /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
